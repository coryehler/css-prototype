# New Project

## Build

### Features

- Dynamic [HTML templating](https://github.com/lukeaskew/gulp-fabricate)
- Sass [compilation](https://github.com/sass/node-sass), [vendor prefixing](https://github.com/postcss/autoprefixer), and [minification](https://github.com/css/csso)
- JavaScript [module bundling](https://github.com/substack/node-browserify), [optimization](https://github.com/mishoo/UglifyJS2), and [linting](https://www.npmjs.com/package/jshint)
- Image [optimization](https://github.com/imagemin/imagemin)
- Code formatting rules via [EditorConfig](http://editorconfig.org/) and [JS Beautifier](https://github.com/beautify-web/js-beautify)

### Local development

Start a local development environment:

```
$ npm start
```

This will compile files, then watch for changes to recompile.

This will also start a server at [http://localhost:3000](http://localhost:3000) that will live-reload when changes are detected.

### Build for distribution

```
$ npm run build
```

In addition to the standard build tasks, this minifies CSS and uglifies JavaScript.

## Configuration

Configuration values should be defined in the `config` object in the root-level `package.json` file.

Available options, with defaults shown:

```json
"templates": {
  "src": "src/templates/pages/**/*.html",
  "dest": "dist/",
  "watch": "src/templates/**/*",
  "options": {}
},
"scripts": {
  "src": "./src/assets/scripts/main.js",
  "vendor": [],
  "dest": "dist/assets/scripts",
  "watch": "src/assets/scripts/**/*"
},
"styles": {
  "src": "src/assets/styles/main.scss",
  "dest": "dist/assets/styles",
  "watch": "src/assets/styles/**/*",
  "browsers": [
    "last 1 version"
  ]
},
"images": {
  "src": "src/assets/images/**/*",
  "dest": "dist/assets/images",
  "watch": "src/assets/images/**/*"
}
```

### Definitions

#### {task}.src 
 
Type: `String` or `Array`  
Source files for task; defined as glob.

#### {task}.dest

Type: `String`  
Output destination.

#### {task}.watch

Type: `String` or `Array`  
Files that should trigger recompilation/reload when changed; defined as glob.

### templates.options

Type: `Object`  
Settings passed to [gulp-fabricate](http://github.com/lukeaskew/gulp-fabricate)  
Default:

```json
{
  "layout": "default",
  "layouts": "src/templates/layouts/**/*",
  "materials": "src/templates/materials/**/*",
  "data": "src/data/**/*.json"
}
```

`layout` - name of your default layout

`layouts`, `materials`, `data` - glob patterns for assembly parts, respectively. 

### scripts.vendor

Type: `Array`  
File globs for vendor scripts to be concatenated.

### styles.browsers

Type: `Array`  
Value passed to [Autoprefixer](https://github.com/postcss/autoprefixer#browsers)

## Template Assembly

Templates are assembled using [gulp-fabricate](http://github.com/lukeaskew/gulp-fabricate).
