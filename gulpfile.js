// modules
var _ = require('lodash');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('browserify');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var del = require('del');
var fabricate = require('gulp-fabricate');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var gutil = require('gulp-util');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var path = require('path');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var streamqueue = require('streamqueue');
var stylish = require('jshint-stylish');
var uglify = require('gulp-uglify');


// config defaults
var defaults = {
	templates: {
		src: 'src/**/*.html',
		dest: 'dist/',
		watch: ['src/**/*.html', 'src/data/**/*.json'],
		options: {}
	},
	scripts: {
		src: './src/scripts/main.js',
		dest: 'dist/scripts',
		watch: 'src/scripts/**/*'
	},
	styles: {
		src: 'src/styles/main.scss',
		dest: 'dist/styles',
		watch: 'src/**/*.scss',
		browsers: ['last 1 version']
	},
	images: {
		src: 'src/images/**/*',
		dest: 'dist/images',
		watch: 'src/images/**/*'
	},
	fonts: {
		src: 'src/elements/font/**/*',
		dest: 'dist/elements/font',
		watch: 'src/elements/font/**/*'
	}
};

// merge user with default config
var config = _.merge(defaults, require('./package.json').config);
config.dev = gutil.env.dev;


// clean
gulp.task('clean', function (cb) {
	del(['dist'], cb);
});


// templates
gulp.task('templates', function () {
	return gulp.src(config.templates.src)
		.pipe(fabricate(config.templates.options))
		.on('error', function (error) {
			gutil.log(gutil.colors.red(error));
			this.emit('end');
		})
		.pipe(gulp.dest(config.templates.dest));
});


// scripts
gulp.task('scripts', function () {

	var filename = path.basename(config.scripts.src);

	var main = function () {
		return browserify(config.scripts.src).bundle()
			.on('error', function (error) {
				gutil.log(gutil.colors.red(error));
				this.emit('end');
			})
			.pipe(source(filename));
	};

	var vendor = function () {
		return gulp.src(config.scripts.vendor)
			.pipe(concat('vendor.js'));
	};

	return streamqueue({ objectMode: true }, vendor(), main())
		.pipe(streamify(concat(filename)))
		.pipe(gulpif(!config.dev, streamify(uglify())))
		.pipe(gulp.dest(config.scripts.dest));

}); 

// styles
gulp.task('styles', function () {
	return gulp.src(config.styles.src)
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(autoprefixer({
			browsers: config.styles.browsers
		}))
		.pipe(gulpif(!config.dev, csso()))
		.pipe(gulp.dest(config.styles.dest));
});


// images
gulp.task('images', function () {
	return gulp.src(config.images.src)
		.pipe(imagemin({
			progressive: true,
			interlaced: true
		}))
		.pipe(gulp.dest(config.images.dest));
});

// fonts
gulp.task('fonts', function () {
	return gulp.src(config.fonts.src)
		.pipe(gulp.dest(config.fonts.dest));
});


// server
gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: config.templates.dest
		},
		notify: false,
		logPrefix: 'BrowserSync'
	});
});


// watch
gulp.task('watch', ['default', 'browser-sync'], function () {
	gulp.watch(config.images.watch, ['images', browserSync.reload]);
	gulp.watch(config.fonts.watch, ['fonts', browserSync.reload]);
	gulp.watch(config.scripts.watch, ['scripts', browserSync.reload]);
	gulp.watch(config.styles.watch, ['styles', browserSync.reload]);
	gulp.watch(config.templates.watch, ['templates', browserSync.reload]);
});


// default build task
gulp.task('default', ['clean'], function () {

	// define build tasks
	var tasks = [
		'templates',
		'scripts',
		'styles',
		'images',
		'fonts'
	];

	// run build
	runSequence(tasks, function () {
		if (config.dev) {
			gulp.start('watch');
		}
	});

});
