$(function () {
	"use strict";

	var defaultTransitionSpeed = 500,
		openClass = "open",
		stayOpenClass = "always-open",
		transitionClass = "accordion-transitioning",
		lastFocus;

	function getElements(event) {
		/**
		 * NOTE: we're using .children() / .parent() references here instead of .find() / .closest()
		 * for the case where there is an accordion inside of another accordion to make sure that we're
		 * looking at the correct element.
		 */
		var $accordion = $(event.target).closest(".fabric-c-accordion"),
			$header = $accordion.children("header").children(".fabric-c-accordion__header"),
			$container = $accordion.children(".fabric-c-accordion__content"),
			$group = $accordion.parent(".fab-accordion-group"),
			transitionSpeed = $accordion.attr("data-prevent-transition") ? 0 : defaultTransitionSpeed;

		return {
			"$header": $header,
			"$accordion": $accordion,
			"$container": $container,
			"$group": $group,
			"isOpen": $accordion.hasClass(openClass),
			"isPersistent": $accordion.attr("data-persist") || false,
			"transitionSpeed": transitionSpeed
		};
	}

	function openAccordion(event) {
		var ele = getElements(event);
		lastFocus = document.activeElement;

		if (!ele.isOpen) {
			ele.$accordion.addClass("accordion-opening");
			ele.$accordion.trigger("fab-accordion-opened-start");
			ele.$container.slideDown(ele.transitionSpeed, function afterOpenAccordionAnim() {
				ele.$accordion.addClass(openClass);
				ele.$header.attr("aria-expanded", true);
				ele.$container.attr("aria-hidden", false);
				ele.$accordion.trigger("fab-accordion-opened");
				ele.$group.removeClass(transitionClass);
				ele.$accordion.removeClass("accordion-opening");
			});

			// If this accordion is persistant, save the state
			if (ele.isPersistent) {
				UU.save("components.accordion." + ele.$accordion.attr("id") + ".model.isOpen", true);
			}

			// If this accordion is in a group, trigger the sibling accordions to close
			if (ele.$group.length > 0) {
				ele.$group.addClass(transitionClass);
				ele.$accordion.siblings().trigger("close-accordion");
				//Allow time for all sibling accordions to close, including transitions
				setTimeout(function (){
					ele.$group.trigger("fab-accordion-group-toggled");
				},1000);

			}
		}
	}

	function closeAccordion(event) {
		var ele = getElements(event);

		if (ele.isOpen && !ele.$accordion.hasClass(stayOpenClass)) {
			// If the accordion is in a group and the group requires one to be open, don't close
			if (ele.$group.length === 0 || !ele.$group.hasClass(stayOpenClass) || ele.$group.hasClass(transitionClass)) {
				ele.$accordion.trigger("fab-accordion-closed-start");
				ele.$container.slideUp(ele.transitionSpeed, function afterCloseAccordionAnim() {
					ele.$accordion.removeClass(openClass);
					ele.$header.attr("aria-expanded", false);
					ele.$container.attr("aria-hidden", true);
					ele.$accordion.trigger("fab-accordion-closed");
				});
			}

		}

		if (lastFocus) {
			lastFocus.focus();
		}
	}

	function toggleFocus(event) {
		var ele = getElements(event);

		ele.$accordion.toggleClass("focused", event.type === "focusin");
	}

	function toggleAccordion(event) {
		var ele = getElements(event);

		if (ele.isOpen) {
			closeAccordion(event);
		} else {
			openAccordion(event);
		}
	}

	$(".fabric-c-accordion .fabric-c-accordion__header").on("click", toggleAccordion);
});